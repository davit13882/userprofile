package com.example.homework5osadze

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        loginButton.setOnClickListener {
            authorization()
        }


    }

    private fun authorization() {
        val email = emailfield.text.toString()
        val password = passwordfield.text.toString()


        if(email.isEmpty() && password.isEmpty()) {
            Toast.makeText(this, "Please fill all fields!", Toast.LENGTH_SHORT).show()
        }
        else {
            val intent = Intent(this, UserProfile::class.java)
            startActivity(intent)
        }

    }
}