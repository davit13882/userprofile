package com.example.homework5osadze

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_user_profile.*

class UserProfile : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)

        init()
    }
    private fun init() {
        Glide.with(this)
            .load("https://lh3.googleusercontent.com/proxy/L-rrm1SyOxv-VU7AMdxUy1WKy3fXCndO9xz9TjMQQwCn6aUalwtr0q7y4np1UWYnFZ_Id4HUMtz4i45V4j-i87vkejRagLtNA4WoU6PeNV50zaGd1K0lqEOrw-c")
            .into(profileImage)

        Glide.with(this)
            .load("https://i.pinimg.com/originals/b6/d9/e4/b6d9e4bb3642d036a207f7a83b2f9128.jpg")
            .into(coverImage)







    }
}